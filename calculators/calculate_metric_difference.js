
var dHash = new DHash();
var previousValueList = {};

function execute(metricData,javascriptResultSetHelper)
{	     
	  for ( i = 0; i < metricData.length; i ++ )
	  {		
		var metric = metricData[i].agentMetric.attributeURL;
		var agent = metricData[i].agentName.processURL;
		var value = metricData[i].timeslicedValue.value;
		var frequency = metricData[i].frequency;
		var isAbsent = metricData[i].timeslicedValue.dataIsAbsent();
		var key = agent;		
		var previousValue;
		
    if (!isAbsent) {    		
        if ( dHash.hasItem( key ) ){
          previousValue = dHash.getItem( key );
        }else{
          previousValue = 0;
        }        
        var difference = Math.abs(value - previousValue);
        dHash.setItem( key , difference );      
      } 
	  }

    var newMetricName = agent + "|" + metric + "Diff";
    javascriptResultSetHelper.addMetric(newMetricName, dHash.getItem( key  ), javascriptResultSetHelper.kIntegerFluctuatingCounter, frequency);

	  return javascriptResultSetHelper;
}

function getAgentRegex()
{
  return ".*";
}


function getMetricRegex()
{
  return "JMX\\|jboss\\.web\\|name=http(.*)\\|type=GlobalRequestProcessor:requestCount";
}

function getFrequency()
{
  return 15;
}

function runOnMOM()
{
  return false;
}

function DHash()
{
  this.length = 0;
  this.items = new Array();
  for (var i = 0; i < arguments.length; i += 2) {
    if (typeof(arguments[i + 1]) != 'undefined') {
      this.items[arguments[i]] = arguments[i + 1];
      this.length++;
    }
  }
   
  this.removeItem = function(in_key)
  {
    var tmp_previous;
    if (typeof(this.items[in_key]) != 'undefined') {
      this.length--;
      var tmp_previous = this.items[in_key];
      delete this.items[in_key];
    }
    return tmp_previous;
  }

  this.getItem = function(in_key) {
    return this.items[in_key];
  }

  this.setItem = function(in_key, in_value)
  {
    var tmp_previous;
    if (typeof(in_value) != 'undefined') {
      if (typeof(this.items[in_key]) == 'undefined') {
        this.length++;
      }
      else {
        tmp_previous = this.items[in_key];
      }

      this.items[in_key] = in_value;
    }
    return tmp_previous;
  }

  this.hasItem = function(in_key)
  {
    return typeof(this.items[in_key]) != 'undefined';
  }

  this.clear = function()
  {
    for (var i in this.items) {
      delete this.items[i];
    }
    this.length = 0;
  }
}

//FIN SCRIPT
var isDevel=true;
if(isDevel){
  console.log(
      execute( require('../dummy_data/data1.js').metricData,  require('../helpers/wily_helper.js').javascriptResultSetHelper )
  );
}
